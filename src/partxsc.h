#ifndef PARTXSC_H
#define PARTXSC_H  1

#include "smatrix.h"

#define EMAT(s, n, i, j) ((s)[(i) * (n) + (j)])

int partial_xsec(struct Header *hp, struct SMatrix *sp, double *pcs);

#endif
