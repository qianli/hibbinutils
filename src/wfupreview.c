/* wfupreview.c -- list channels included in a Hibridon wfu file. */

/* Written by Qianli Ma */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <getopt.h>

#include "typemap.h"

#define PROGRAM_NAME "wfupreview"
#define VERSION "0.1-alpha2"
#define AUTHORS "Qianli Ma"


static int_h nch_print=0;


static void show_help(void)
{
  printf("Usage: %s [OPTION]... [FILE]...\n", PROGRAM_NAME);
  fputs("\
List channels included in a Hibridon wfu file.\n\
\n\
", stdout);

  fputs("Arguments:\n\
  -n, --channels=K       output the asymtotically lowest K channels\n\
      --version          show version information and exit\n\
  -h, --help             show this message and exit\n\
", stdout);

  exit(0);
}


static void show_version(void)
{
  fprintf(stdout, "%s %s\n\nWritten by %s.\n", 
	  PROGRAM_NAME, VERSION, AUTHORS);
  exit(0);
}


static void process_file(const char* filename)
{
  FILE *fp;
  char_h str[48];
  int_h nch;
  int_h *j, *l, *i;
  double_h *e;
  int n;
  int_h nch_print_this;

  if ((fp = fopen(filename, "r")) == NULL)
    {
      fprintf(stderr, "error: file %s cannot be opened.\n\n", filename);
      return;
    }

  fprintf(stdout, "*** READ FILE %s\n", filename);

  fseeko(fp, 0xb0, SEEK_SET);
  fread(&nch, sizeof(int_h), 1, fp);

  nch_print_this = (nch_print == 0) ? nch :
    ((nch > nch_print) ? nch_print : nch);

  j = (int_h *)malloc(nch * sizeof(int_h));
  l = (int_h *)malloc(nch * sizeof(int_h));
  i = (int_h *)malloc(nch * sizeof(int_h));
  e = (double_h *)malloc(nch * sizeof(double_h));

  fseeko(fp, 0xf0, SEEK_SET);
  fread(j, sizeof(int_h), nch, fp);
  fread(l, sizeof(int_h), nch, fp);
  fread(i, sizeof(int_h), nch, fp);
  fread(e, sizeof(double_h), nch, fp);

  fputs("     J     L     I          E\n", stdout);
  for (n = 0; n != nch_print_this; n++)
    {
      fprintf(stdout, "%6ld%6ld%6ld%11.3f\n", 
	      j[n], l[n], i[n], 219474.6 * e[n]);
    }
  fprintf(stdout, "*** FILE CONTAINS %ld CHANNELS, %ld PRINTED.\n",
	  nch, nch_print_this);
  fputc('\n', stdout);

  return;
}


int main(int argc, char *argv[])
{
  int c;
  static struct option long_options[] =
    {
      {"version", no_argument, 0, 0},
      {"help", no_argument, 0, 'h'},
      {"channels", required_argument, 0, 'n'},
      {0, 0, 0, 0}
    };

  while (1)
    {
      int option_index = 0;
      c = getopt_long(argc, argv, "hn:", long_options, &option_index);
      if (c == -1) break;
      switch (c)
	{
	case 0:
	  if (option_index == 0)
	    show_version();
	  break;
	case 'h':
	  show_help();
	  break;
	case 'n':
	  nch_print = atoi(optarg);
	  if (nch_print <= 0)
	    {
	      fputs("warning: invalid argument for number of channels\n",
		    stdout);
	      nch_print = 0;
	    }
	  break;
	case '?':
	  break;
	default:
	  abort();
	}
    }

  while (optind < argc)
    {
      process_file(argv[optind++]);
    }

  return 0;
}
