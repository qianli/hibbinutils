/* shib44.c

   Library reading stream s-matrix files of hibridon 4.4

   Authors: Qianli Ma

   Revision: Aug 22, 2012

*/


#include <string.h>
#include "shib44.h"


int read_header_hib44(FILE *fp, struct Header *head)
{
  struct SMTHeaderHIB44 h;
  size_t count;
  char_f meta[9];

  if (fread(&h, sizeof(struct SMTHeaderHIB44), 1, fp) != 1)
    goto read_error;
  memcpy(head, (void *)((char *)&h + sizeof(int_f)),
	 sizeof(struct SMTHeaderHIB44) - sizeof(int_f));

  count = (size_t)h.nlevel;
  head->jlev = (int_f *)malloc(sizeof(int_f) * count);
  head->ilev = (int_f *)malloc(sizeof(int_f) * count);
  head->elev = (double_f *)malloc(sizeof(double_f) * count);
  head->jout = (int_f *)malloc(sizeof(int_f) * (size_t)h.nnout);

  if (head->jlev == NULL || head->ilev == NULL || head->elev == NULL ||
      head->jout == NULL)
    {
      fputs("read_header_hib44: error: insufficient memory.\n", stderr);
      return -2;
    }

  if (fread(head->jlev, sizeof(int_f), count, fp) != count) goto read_error;
  if (fread(head->ilev, sizeof(int_f), count, fp) != count) goto read_error;
  if (fread(head->elev, sizeof(double_f), count, fp) != count) goto read_error;
  if (fread(head->jout, sizeof(int_f), h.nnout, fp) != h.nnout)
    goto read_error;
  if (fread(meta, sizeof(char_f), 8, fp) != 8) goto read_error;

  return 0;

 read_error:
  fputs("read_header_hib44: error: read error.\n", stderr);
  return -2;
}


int read_smatrix_hib44(FILE *fp, struct SMatrix *smt, int is_j12)
{
  struct SHeaderHIB44 h;
  size_t pack_len;
  char_f meta[9];
  int_f irow, icol, iseq;
  double_f *stemp;

  if (fread(&h, sizeof(struct SHeaderHIB44), 1, fp) == 0)
    return EOF;
  smt->jtot = h.jtot;
  smt->jlpar = h.jlpar;
  smt->nu = h.nu;
  smt->nopen = h.nopen;
  smt->length = h.length;
  smt->nnout = h.nnout;

  smt->j = (int_f *)malloc(sizeof(int_f) * h.length);
  smt->i = (int_f *)malloc(sizeof(int_f) * h.length);
  smt->l = (int_f *)malloc(sizeof(int_f) * h.length);
  if (is_j12)
    smt->j12 = (int_f *)malloc(sizeof(int_f) * h.length);
  else
    smt->j12 = NULL;
  pack_len = h.length * (h.length + 1) / 2;
  smt->s = (double complex *)malloc(sizeof(double complex) * pack_len);
  stemp = (double_f *)malloc(sizeof(double_f) * 2 * pack_len);

  if (smt->j == NULL || smt->i == NULL || smt->l == NULL ||
      smt->s == NULL)
    {
      fputs("read_smatrix_hib44: error: insufficient memory.\n", stderr);
      return -2;
    }

  if (fread(smt->j, sizeof(int_f), h.length, fp) != h.length)
    goto read_error;
  if (fread(smt->l, sizeof(int_f), h.length, fp) != h.length)
    goto read_error;
  if (fread(smt->i, sizeof(int_f), h.length, fp) != h.length)
    goto read_error;
  if (is_j12)
    {
      if (fread(smt->j12, sizeof(int_f), h.length, fp) != h.length)
	goto read_error;
    }

  if (fread(stemp, sizeof(double_f), pack_len * 2, fp) != pack_len * 2)
    goto read_error;
  for (icol = 1; icol <= h.length; icol++)
    for (irow = 1; irow <= icol; irow++)
      {
	iseq = icol * (icol - 1) + irow;
	ELEMTRIG(smt->s, irow - 1, icol - 1) = *(stemp + iseq - 1)
	  + *(stemp + icol + iseq - 1) * I;
      }

  if (fread(meta, sizeof(char_f), 8, fp) != 8)
    goto read_error;

  return 0;

 read_error:
  fputs("read_smatrix_hib44: error: read error.\n", stderr);
  return -2;
}


int write_header_hib44(FILE *fp, struct Header *head)
{
  int_f len_hdr;

  if (fwrite(MAGIC_NUMBER_HIB44, 8, 1, fp) != 1)
    goto write_error;
  /* the following is a dirty estimation and needs improvement */
  len_hdr = 0x118 + 24 * head->nlevel + 8 * head->nnout;

  if (fwrite(&len_hdr, sizeof(int_f), 1, fp) != 1)
    goto write_error;
  if (fwrite(head,
	     sizeof(struct Header) - 3 * sizeof(int_f*) - sizeof(double_f*),
	     1, fp) != 1)
    goto write_error;
  if ((fwrite(head->jlev, sizeof(int_f), head->nlevel, fp) != head->nlevel) ||
      (fwrite(head->ilev, sizeof(int_f), head->nlevel, fp) != head->nlevel) ||
      (fwrite(head->elev, sizeof(double_f), head->nlevel, fp) != head->nlevel)
      || (fwrite(head->jout, sizeof(int_f), head->nnout, fp) != head->nnout))
    goto write_error;
  if (fwrite("ENDOFHDR", 8, 1, fp) != 1)
    goto write_error;

  return 0;

 write_error:
  fputs("write_header_hib44: error: write error.\n", stderr);
  return -2;
}


int write_smatrix_hib44(FILE *fp, struct SMatrix *smt)
{
  int_f is_j12, len_smt, irow, icol;
  double dwrite;

  is_j12 = smt->j12 ? 1 : 0;
  len_smt = sizeof(int_f) * (7 + (3 + is_j12) * smt->length)
    + sizeof(double_f) * smt->length * (smt->length + 1) + 8;

  if (fwrite(&len_smt, sizeof(int_f), 1, fp) != 1)
    goto write_error;
  if (fwrite(smt, sizeof(int_f), 6, fp) != 6)
    goto write_error;

  if ((fwrite(smt->j, sizeof(int_f), smt->length, fp) != smt->length) ||
      (fwrite(smt->l, sizeof(int_f), smt->length, fp) != smt->length) ||
      (fwrite(smt->i, sizeof(int_f), smt->length, fp) != smt->length))
    goto write_error;

  if (is_j12 &&
      (fwrite(smt->j12, sizeof(int_f), smt->length, fp) != smt->length))
    goto write_error;

  for (icol = 0; icol < smt->length; icol++)
    {
      for (irow = 0; irow <= icol; irow++)
	{
	  dwrite = creal(ELEM(smt->s, irow, icol));
	  if (fwrite(&dwrite, sizeof(double), 1, fp) != 1)
	    goto write_error;
	}
      for (irow = 0; irow <= icol; irow++)
	{
	  dwrite = cimag(ELEM(smt->s, irow, icol));
	  if (fwrite(&dwrite, sizeof(double), 1, fp) != 1)
	    goto write_error;
	}
    }

  if (fwrite("ENDOFSMT", 8, 1, fp) != 1)
    goto write_error;

  return 0;

 write_error:
  fputs("error: write_smatrix_hib44: write error.\n", stderr);
  return -2;
}
