/* sqmc01.h

   Header file for the library reading s-matrix file written by the C
   code of Q. Ma 

   This library handles the file with magic number

   80 53 4d 54 02 01 00 00

   Authors: Qianli Ma

   Revision: Jan 17, 2012 */

#ifndef SQMC01_H
#define SQMC01_H  1

#include <stdio.h>
#include <stdlib.h>
#include "smatrix.h"

#define LEN_OF_DATE_QMC01  20
#define LEN_OF_LABEL_QMC01 48
#define LEN_OF_POTNM_QMC01 48

#define MAGIC_NUMBER_QMC01 "\x80SMT\x02\x01\x00\x00"
#define RESERVED_CRC_QMC01 "\xFF\xFF\xFF\xFFSMT\x00"


struct SMTHeaderQMC01
{
  char_f date[LEN_OF_DATE], label[LEN_OF_LABEL], potnm[LEN_OF_POTNM];
  double_f ered, rmu;
  int_f csflag, flaghf, flagsu, twomol, nucros;
  int_f jfirst, jfinal, jtotd, numin, numax;
  int_f nud, nlevel, nlevop, nnout, ibasty;
};


struct SHeaderQMC01
{
  int_f jtot, jlpar, nu, nopen, length, nnout;
};


int read_header_qmc01(FILE *fp, struct Header *head);

int read_smatrix_qmc01(FILE *fp, struct SMatrix *smt);

#endif
