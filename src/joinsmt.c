/* joinsmt.c
 * 
 * Join hib44 s-matrix files.
 * 
 * Author: Qianli Ma
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

static void version(void)
{
    fputs("joinsmt 0.1-alpha2\n\n", stdout);
    fputs("Written by Qianli Ma\n", stdout);
    
    exit(0);
}


static struct option const long_opts[] =
    {
        {"output", required_argument, 0, 'o'},
        {"version", no_argument, 0, 0},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };
static const char *opt_string = "oh?";

static void usage(void)
{
    fputs("usage: joinsmt [OPTION]... SMT_FILE...\n", stdout);
    fputs("Join Hibridon 4.4 S-matrix files\n", stdout);
    putchar('\n');
    fputs("Options:\n", stdout);
    
    fputs("\
      --output=FILE            set output file, default is joined.smt\n\
      --version                show version number and exit\n\
  -h, --help                   show this message and exit\n\
", stdout);
    
    exit(0);
}


static char *output_file = 0;

// int run_joinsmt(int argc, char **argv, char *output_file);

int main(int argc, char **argv)
{    
    int c;
    while (1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, opt_string, long_opts, &option_index);
        if (c == -1) break;
        
        switch (c)
        {
            case 0:
                if (strcmp("version", (char *)long_opts[option_index].name) == 0)
                    version();
                break;
            
            case 'o':
                output_file = optarg;
                break;
                
            case 'h': case '?':
                usage();
                break;
            
            default:
                fputs("?? getopt error ??\n", stderr);
        }
    }
    
    if (optind == argc) usage();
    
    if (output_file == 0) output_file = "joined.smt";
    printf("Output file set to: %s\n", output_file);

    run_joinsmt(argc - optind + 1, &argv[optind - 1], output_file);
    
    exit(0);
}
