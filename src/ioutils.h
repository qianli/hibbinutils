/* ioutils.h

   Header file for data input/output utilities.

   Authors: Qianli Ma

   Revision: Jan 18, 2012 */


#ifndef IOUTILS_H
#define IOUTILS_H  1

#include <stdio.h>
#include <stdlib.h>
#include "smatrix.h"

int print_header(struct Header *ph, FILE *fp);

#endif
