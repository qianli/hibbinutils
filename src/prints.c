/* prints.c

   Print contents from s-matrix files.

   Authors: Qianli Ma
   
   Revision: Jan 18, 2012 */


#include <unistd.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include "smatrix.h"
#include "sread.h"
#include "ioutils.h"

static void version(void)
{
  fputs("prints 0.1-alpha1\n\n", stdout);
  fputs("Written by Qianli Ma\n", stdout);

  exit(0);
}


static struct option const long_opts[] =
  {
    {"channels", no_argument, NULL, 'c'},
    {"s-matrix", no_argument, NULL, 's'},
    {"s-square", no_argument, NULL, 'S'},
    {"t-square", no_argument, NULL, 'T'},
    {"version", no_argument, NULL, 0},
    {"help", no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}
  };
static const char *opt_string = "sSTh?";


enum ofmt_t {
  O_SUMMARY_ONLY = 0,
  O_CHANNELS = 1,
  O_S_MATRIX = 2,
  O_SSQ_MATRIX = 4,
  O_TSQ_MATRIX = 8
};


static void usage(void)
{
  fputs("usage: prints [OPTION]... SMT_FILE...\n", stdout);
  fputs("Print the contents of SMT_FILE(s)\n", stdout);
  putchar('\n');
  fputs("Options:\n", stdout);

  fputs("\
  -c, --channels               print list of channels\n\
  -s, --s-matrix               print full S-matrices\n\
  -S, --s-square               print the modulus square of S-matrices\n\
  -T, --t-square               print the modulus square of T-matrices\n\
      --version                show version number and exit\n\
  -h, --help                   show this message and exit\n\
", stdout);

  exit(0);
}


static void print_smatrix(const struct SMatrix *smtp)
{
  int i, j;
  double complex selem;
  
  fputs("LOWER TRIANGLE OF S-MATRIX:\n", stdout);
  for (i = 0; i != smtp->length; i++)
    {
      for (j = 0; j != i + 1; j++)
	{
	  selem = ELEM(smtp->s, i, j);
	  printf("%+.6e %+.6ei    ", creal(selem), cimag(selem));
	}
      putchar('\n');
    }
  putchar('\n');
  putchar('\n');
  return;
}


static void print_ssqmatrix(const struct SMatrix *smtp)
{
  int i, j;
  double selem;
  
  fputs("LOWER TRIANGLE OF THE MODULUS SQUARE OF S-MATRIX:\n", stdout);
  for (i = 0; i != smtp->length; i++)
    {
      for (j = 0; j != i + 1; j++)
	{
	  selem = cabs(ELEM(smtp->s, i, j));
	  printf("%.6e  ", selem * selem);
	}
      putchar('\n');
    }
  putchar('\n');
  return;
}


static void print_tsqmatrix(const struct SMatrix *smtp)
{
  int i, j;
  double selem;
  
  fputs("LOWER TRIANGLE OF THE MODULUS SQUARE OF T-MATRIX:\n", stdout);
  for (i = 0; i != smtp->length; i++)
    {
      for (j = 0; j != i + 1; j++)
	{
	  selem = (i == j) ? cabs(1.0 - ELEM(smtp->s, i, j)) :
	    cabs(ELEM(smtp->s, i, j));
	  printf("%.6e  ", selem * selem);
	}
      putchar('\n');
    }
  putchar('\n');
  return;
}


static int process(const char* path, int ofmt)
{
  FILE *fp;
  struct Header header;
  struct SMatrix smt;
  int i, stat, is_j12;

  fprintf(stdout, "S-MATRIX FILE: %s\n\n", path);

  fp = fopen(path, "r");

  if (read_header(fp, &header)) goto clean_up;
  print_header(&header, stdout);
  putchar('\n');

  is_j12 = check_j12(header.ibasty);

  while ((stat = read_smatrix(fp, &smt)) == 0)
    {
      printf("S-MATRIX FOR JTOT=%ld, JLPAR=%ld, LENGTH=%ld.\n",
	     smt.jtot, smt.jlpar, smt.length);

      if (ofmt & O_CHANNELS)
	{
	  for (i = 0; i != smt.length; i++)
	    if (is_j12)
	      {
		printf("  CHANNEL #%d, j1=%ld, j2=%ld, j12=%ld, i=%ld, l=%ld\n",
		       i, smt.j[i] / 10, smt.j[i] % 10, smt.j12[i], smt.i[i],
		       smt.l[i]);
	      }
	    else
	      {
		printf("  CHANNEL #%d, j=%ld, i=%ld, l=%ld\n", i, smt.j[i],
		       smt.i[i], smt.l[i]);
	      }
	  putchar('\n');
	}

      if (ofmt & O_S_MATRIX) print_smatrix(&smt);
      if (ofmt & O_SSQ_MATRIX) print_ssqmatrix(&smt);
      if (ofmt & O_TSQ_MATRIX) print_tsqmatrix(&smt);

      free_smatrix(&smt);
    }

  if (stat == -2) free_smatrix(&smt);

 clean_up:
  free_header(&header);
  if (fp) fclose(fp);

  return 0;
}


int main(int argc, char **argv)
{
  int c, ofmt = 0;

  while (1)
    {
      int option_index = 0;
      c = getopt_long(argc, argv, opt_string, long_opts, &option_index);
      if (c == -1) break;

      switch (c)
	{
	case 0:
	  if (strcmp("version", (char *)long_opts[option_index].name) == 0)
	      version();
	  break;

	case 'c':
	  ofmt |= O_CHANNELS;
	  break;

	case 's':
	  ofmt |= O_S_MATRIX;
	  break;

	case 'S':
	  ofmt |= O_SSQ_MATRIX;
	  break;

	case 'T':
	  ofmt |= O_TSQ_MATRIX;
	  break;

	case 'h': case '?':
	  usage();
	  break;

	default:
	  fputs("?? getopt error ??\n", stderr);
	}
    }

  if (optind == argc) usage();

  while (optind < argc)
    process(argv[optind++], ofmt);

  exit(0);
}
