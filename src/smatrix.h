/* smatrix.h

   Header file for general S-matrix I/O

   Authors: Qianli Ma

   Revision: Jan 17, 2012 */


#ifndef SMATRIX_H
#define SMATRIX_H  1

#include <stdint.h>		/* Requires C99 support */
#include <complex.h>

#define LEN_OF_DATE 20
#define LEN_OF_LABEL 48
#define LEN_OF_POTNM 48

#define CONV_ENERGY 219474.63137078
#define CONV_MASS   1822.8884847700403
#define CONV_AU_ANG 0.2800285205570702
#define PI 3.1415926536

/* Get an element from the lower angle of a matrix as an 1-D array */
#define ELEM(s, i, j) (((i) < (j)) ? ((s)[(j) * ((j) + 1) / 2 + (i)])	\
		       : ((s)[(i) * ((i) + 1) / 2 + (j)]))
#define ELEMTRIG(s, i, j) ((s)[(j) * ((j) + 1) / 2 + (i)])

typedef int64_t int_f;		/* With -i8, fortran use 64-bit int */
typedef double double_f;
typedef char char_f;

struct Header
{
  char_f date[LEN_OF_DATE], label[LEN_OF_LABEL], potnm[LEN_OF_POTNM];
  double_f ered, rmu;
  int_f csflag, flaghf, flagsu, twomol, nucros;
  int_f jfirst, jfinal, jtotd, numin, numax;
  int_f nud, nlevel, nlevop, nnout, ibasty;
  int_f *jlev, *ilev;
  double_f *elev;
  int_f *jout;
};

struct SMatrix
{
  int_f jtot, jlpar, nu, nopen, length, nnout;
  int_f *j, *i, *l, *j12;
  double complex *s;
};

#endif
