/* rmj12smt.c

   Removes j12 from a smt file, extract j1 as j, and change ibasty to 99.

   Authors: Qianli Ma

   Revision: Aug 23, 2012

 */


#include <stdio.h>
#include "smatrix.h"
#include "sread.h"


static void process_stream(FILE *ifp, FILE *ofp)
{
  struct Header header;
  struct SMatrix smt;
  int i, stat;

  if (read_header(ifp, &header)) goto clean_up;
  header.ibasty = 99;
  header.twomol = 0;
  for (i = 0; i < header.nlevel; i++)
    header.jlev[i] /= 10;
  for (i = 0; i < header.nnout; i++)
    header.jout[i] /= 10;
  if (write_header(ofp, &header, 2)) goto clean_up;

  while ((stat = read_smatrix(ifp, &smt)) == 0)
    {
      free(smt.j12);
      smt.j12 = NULL;
      for (i = 0; i < smt.length; i++)
	smt.j[i] /= 10;
      write_smatrix(ofp, &smt, 2);
      free_smatrix(&smt);
    }

 clean_up:
  free_header(&header);
}


static void process_file(const char *ifn, const char *ofn)
{
  FILE *ifp, *ofp;
  ifp = fopen(ifn, "r");
  ofp = fopen(ofn, "w");
  process_stream(ifp, ofp);
  fclose(ifp);
  fclose(ofp);
}


int main(int argc, char *argv[])
{
  process_file(argv[1], argv[2]);
  return 0;
}
