/* sqmc01.c

   Library reading s-matrix file written by the C code of Q. Ma

   This library handles the file with magic number

   80 53 4d 54 02 01 00 00

   Authors: Qianli Ma

   Revision: Jan 17, 2012 */


#include <string.h>
#include "sqmc01.h"


int read_header_qmc01(FILE *fp, struct Header *head)
{
  struct SMTHeaderQMC01 h;
  size_t count;
  char_f meta[9];

  if (fread(&h, sizeof(struct SMTHeaderQMC01), 1, fp) != 1)
    goto read_error;
  memcpy(head, &h, sizeof(struct SMTHeaderQMC01));

  count = (size_t)h.nlevel;
  head->jlev = (int_f *)malloc(sizeof(int_f) * count);
  head->ilev = (int_f *)malloc(sizeof(int_f) * count);
  head->elev = (double_f *)malloc(sizeof(double_f) * count);
  head->jout = (int_f *)malloc(sizeof(int_f) * (size_t)h.nnout);

  if (head->jlev == NULL || head->ilev == NULL || head->elev == NULL ||
      head->jout == NULL)
    {
      fputs("read_header_qmc01: error: insufficient memory.\n", stderr);
      return -2;
    }

  if (fread(head->jlev, sizeof(int_f), count, fp) != count) goto read_error;
  if (fread(head->ilev, sizeof(int_f), count, fp) != count) goto read_error;
  if (fread(head->elev, sizeof(double_f), count, fp) != count) goto read_error;
  if (fread(head->jout, sizeof(int_f), h.nnout, fp) != h.nnout)
    goto read_error;
  if (fread(meta, sizeof(char_f), 8, fp) != 8) goto read_error;

  return 0;

 read_error:
  fputs("read_header_qmc01: error: read error.\n", stderr);
  return -2;
}


int read_smatrix_qmc01(FILE *fp, struct SMatrix *smt)
{
  struct SHeaderQMC01 h;
  size_t pack_len;
  char_f meta[9];

  if (fread(&h, sizeof(struct SHeaderQMC01), 1, fp) == 0)
    return EOF;
  memcpy(smt, &h, sizeof(struct SHeaderQMC01));
  
  smt->j = (int_f *)malloc(sizeof(int_f) * h.length);
  smt->i = (int_f *)malloc(sizeof(int_f) * h.length);
  smt->l = (int_f *)malloc(sizeof(int_f) * h.length);
  smt->j12 = NULL;
  pack_len = h.length * (h.length + 1) / 2;
  smt->s = (double complex *)malloc(sizeof(double complex) * pack_len);

  if (smt->j == NULL || smt->i == NULL || smt->l == NULL ||
      smt->s == NULL)
    {
      fputs("read_smatrix_qmc01: error: insufficient memory.\n", stderr);
      return -2;
    }

  if (fread(smt->j, sizeof(int_f), h.length, fp) != h.length)
    goto read_error;
  if (fread(smt->l, sizeof(int_f), h.length, fp) != h.length)
    goto read_error;
  if (fread(smt->i, sizeof(int_f), h.length, fp) != h.length)
    goto read_error;
  if (fread(smt->s, sizeof(double complex), pack_len, fp) != pack_len)
    goto read_error;
  if (fread(meta, sizeof(char_f), 8, fp) != 8)
    goto read_error;

  return 0;

 read_error:
  fputs("read_smatrix_qmc01: error: read error.\n", stderr);
  return -2;
}
