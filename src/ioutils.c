/* ioutils.c

   Data input/output utilities.

   Authors: Qianli Ma

   Revision: Jan 18, 2012 */

#include "ioutils.h"
#include "sread.h"

int print_header(struct Header *ph, FILE *fp)
{
  int i, is_j12;

  fputs("DATE: ", fp);
  for (i = 0; i != LEN_OF_DATE; i++) fputc(ph->date[i], fp);
  fputc('\n', fp);

  fputs("LABEL: ", fp);
  for (i = 0; i != LEN_OF_LABEL; i++) fputc(ph->label[i], fp);
  fputc('\n', fp);

  fputs("POT NAME: ", fp);
  for (i = 0; i != LEN_OF_POTNM; i++) fputc(ph->potnm[i], fp);
  fputc('\n', fp);

  fprintf(fp, "TOTAL ENERGY: %f, REDUCED MASS: %f\n",
	  ph->ered * CONV_ENERGY, ph->rmu / CONV_MASS);
  
  fprintf(fp, "JTOT1: %ld, JTOT2: %ld\n\n", ph->jfirst, ph->jfinal);

  fprintf(fp, "%ld LEVELS, %ld OF WHICH OPEN:\n", ph->nlevel, ph->nlevop);
  is_j12 = check_j12(ph->ibasty);
  for (i = 0; i != ph->nlevop; i++)
    if (is_j12)
      fprintf(fp, "  LEVEL #%d, j1=%ld, j2=%ld, i=%ld, e=%f\n", 
              i, ph->jlev[i] / 10, ph->jlev[i] % 10, 
              ph->ilev[i], ph->elev[i] * CONV_ENERGY);
    else
      fprintf(fp, "  LEVEL #%d, j=%ld, i=%ld, e=%f\n", i, ph->jlev[i],
              ph->ilev[i], ph->elev[i] * CONV_ENERGY);
  fputc('\n', fp);

  return 0;
}
