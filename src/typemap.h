/* Mapping types used in hibridon to c-types */

#ifndef TYPEMAP_H
#define TYPEMAP_H 1

#include <stdint.h>

typedef int64_t int_h;
typedef double double_h;
typedef char char_h;

#endif
