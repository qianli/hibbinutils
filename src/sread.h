/* sread.h

   Header file for reading S-matrix

   Authors: Qianli Ma

   Revision: Jan 17, 2012 */


#ifndef SREAD_H
#define SREAD_H  1

#include <stdio.h>
#include <stdlib.h>
#include "smatrix.h"

#define SVER_QMC01  1
#define SVER_HIB44  2

int check_j12(int_f ibasty);

int read_header(FILE *fp, struct Header *head);

int read_smatrix(FILE *fp, struct SMatrix *smt);

int write_header(FILE *fp, struct Header *head, int s_version);

int write_smatrix(FILE *fp, struct SMatrix *smt, int s_version);

void free_header(const struct Header *head);

void free_smatrix(const struct SMatrix *smt);

#endif
