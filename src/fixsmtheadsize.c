/* fixsmtheadsize.c

   Fix incorrect smt file header size for stream s-matrix files
   generated prior to commit bb8b72f4fcb2 (17-apr-2012).

   usage: fixsmtheadsize [file1 [file2 [...]]]

   Author: Qianli Ma

   Revision: Apr 17, 2012 */

/* This program assumes that 64-bit integers are used */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


int main(int argc, char *argv[])
{
  int i;
  char *filename, char4[5], char8[9];
  int64_t lenhd;
  FILE *fp;

  for (i = 1; i != argc; i++)
    {
      filename = argv[i];
      if ((fp = fopen(filename, "r+")) == NULL)
	{
	  printf("Cannot open file %s\n", filename);
	  continue;
	}
      fread(char8, 8 * sizeof(char), 1, fp);
      char8[8] = '\0';

      if (strncmp(char8, "\200SMT\000\002\000\000", 8))
	{
	  printf("File %s is not a hibridon stream S-matrix file\n", filename);
	  fclose(fp);
	  continue;
	}
      fread(&lenhd, sizeof(int64_t), 1, fp);
      fseek(fp, lenhd - 4, SEEK_SET);
      fread(char4, 4 * sizeof(char), 1, fp);
      char4[4] = '\0';
      if (strncmp(char4, "ENDO", 4) == 0)
	{
	  lenhd += 4;
	  fseek(fp, 8, SEEK_SET);
	  fwrite(&lenhd, sizeof(int64_t), 1, fp);
	  printf("File %s has been fixed\n", filename);
	  fclose(fp);
	  continue;
	}
      else if (strncmp(char4, "FHDR", 4) == 0)
	{
	  printf("File %s is already fixed\n", filename);
	  fclose(fp);
	  continue;
	}
      printf("Unable to fix file %s\n", filename);
      fclose(fp);
    }

  return 0;
}
