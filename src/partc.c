/* partc.c

   Calculate from s-matrix files partial cross section.

   Authors: Qianli Ma
*/


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <math.h>
#include <string.h>
#include "partxsc.h"
#include "sread.h"

#define BOOLEAN(i) ((i) ? 'T' : 'F')

static void version(void)
{
  fputs("partc 0.1-alpha1\n\n", stdout);
  fputs("Written by Qianli Ma\n", stdout);

  exit(0);
}


static struct option const long_opts[] =
  {
    {"version", no_argument, NULL, 0},
    {"help", no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}
  };
static const char *opt_string="h?";


static void usage(void)
{
  fputs("usage: partc [OPTION]... SMT_FILE...\n", stdout);
  fputs("Calculate partial cross sections from SMT_FILE(s)\n\n", stdout);

  fputs("Options:\n", stdout);
  fputs("\
      --version                show version number and exit\n\
  -h, --help                   show this message and exit\n\
", stdout);

  exit(0);
}


static int process(const char* path)
{
  FILE *fsmt, *fpcs;
  char *path_pcs;
  struct Header h;
  struct SMatrix s;

  int nlev, i, j;
  double *pcs, k;

  fsmt = fopen(path, "r");
  path_pcs = (char *)malloc(strlen(path) + 5);
  sprintf(path_pcs, "%s.pcs", path);
  fpcs = fopen(path_pcs, "w");

  read_header(fsmt, &h);
  if (h.csflag)
    {
      fputs("partc: error: type of calculation not supported.\n", stderr);
      exit(1);
    }
  nlev = h.nlevop;
  
  for (i = 0; i != LEN_OF_DATE; i++)
    fputc(h.date[i], fpcs);
  fputc('\n', fpcs);

  for (i = 0; i != LEN_OF_LABEL; i++)
    fputc(h.label[i], fpcs);
  fputc('\n', fpcs);

  for (i = 0; i != LEN_OF_POTNM; i++)
    fputc(h.potnm[i], fpcs);
  fputc('\n', fpcs);

  fprintf(fpcs, "%10.3f  %f\n", h.ered * CONV_ENERGY, h.rmu / CONV_MASS);
  fprintf(fpcs, "  %c  %c  %c  %c  %c\n", BOOLEAN(h.csflag), BOOLEAN(h.flaghf),
	  BOOLEAN(h.flagsu), BOOLEAN(h.twomol), BOOLEAN(h.nucros));
  fprintf(fpcs, " %4ld %4ld %4ld %4ld %4ld %4ld %4ld\n", h.jfirst, h.jfinal,
	  h.jtotd, h.numin, h.numax, 0l, h.nud);
  fprintf(fpcs, " %4ld %4ld\n", h.nlevop, h.nlevop);
  for (i = 0; i != h.nlevop; i++)
    fprintf(fpcs, " %4ld %4ld", h.jlev[i], h.ilev[i]);
  fputc('\n', fpcs);
  for (i = 0; i != h.nlevop; i++)
    fprintf(fpcs, " %e", h.elev[i]);
  fputc('\n', fpcs);

  pcs = (double*)malloc(sizeof(double) * nlev * nlev);
  if (pcs == NULL)
    {
      fputs("partc: error: insufficient memory.\n", stderr);
      exit(1);
    }

  while (read_smatrix(fsmt, &s) != EOF)
    {
      fprintf(fpcs, "%5ld\n", s.jtot);

      memset(pcs, 0, sizeof(double) * nlev * nlev);

      partial_xsec(&h, &s, pcs);

      for (i = 0; i != nlev; i++)
	{
	  fprintf(fpcs, "%3ld%5ld ", h.jlev[i], h.ilev[i]);
	  for (j = 0; j != nlev; j++)
	    {
	      k = EMAT(pcs, nlev, i, j) * CONV_AU_ANG;
	      fprintf(fpcs, "%.6e ", (k > 1e-99) ? k : 0.0);
	    }
	  fputc('\n', fpcs);
	}

      free_smatrix(&s);
    }
  
  free_header(&h);
  free(pcs);
  free(path_pcs);

  fclose(fsmt);
  fclose(fpcs);

  return 0;
}


int main(int argc, char **argv)
{
  int c;

  while (1)
    {
      int option_index = 0;
      c = getopt_long(argc, argv, opt_string, long_opts, &option_index);
      if (c == -1) break;

      switch (c)
	{
	case 0:
	  if (strcmp("version", (char *)long_opts[option_index].name) == 0)
	    version();
	  break;

	case 'h': case '?':
	  usage();
	  break;

	default:
	  fputs("?? getopt error ??\n", stderr);
	}
    }

  if (optind == argc) usage();

  while (optind < argc)
    process(argv[optind++]);

  exit(0);
}
