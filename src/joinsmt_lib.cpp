/* joinsmt.cpp

   Join multiple s-matrix files into one.

   Authors: Qianli Ma
   Revision: Sep 22, 2012
*/

#include <iostream>
#include <iomanip>
#include <limits>
#include <map>
#include <list>
#include <cstdlib>

#include "joinsmt_lib.h"
#include "smatrix.h"
#include "sread.h"


static void
check_files(int argc, char **argv, jfmap_t &jfplus, jfmap_t &jfminus,
	    jlist_t &jlist)
{
  if (argc < 2) return;
  for (int ifile = 1; ifile != argc; ifile ++)
    {
      std::string filename = argv[ifile];
      FILE *ifp = fopen(filename.c_str(), "r");
      struct Header header;
      read_header(ifp, &header);
      struct SMatrix smt;
      while (read_smatrix(ifp, &smt) == 0)
	{
	  jlist.insert(smt.jtot);
	  std::cout << "  found s-matrix from " << filename <<
	    ", jlpar=" << smt.jlpar << ", jtot=" << smt.jtot << std::endl;
	  if (smt.jlpar == 1)
	    {
	      if (jfplus.find(smt.jtot) == jfplus.end())
		jfplus[smt.jtot] = filename;
	      else goto duplicate_entry;
	    }
	  else
	    {
	      if (jfminus.find(smt.jtot) == jfminus.end()) 
		jfminus[smt.jtot] = filename;
	      else goto duplicate_entry;
	    }
	  free_smatrix(&smt);
	}
      free_header(&header);
      fclose(ifp);
    }
  return;
 duplicate_entry:
  std::cerr << "joinsmt: error: duplicate s-matrix found." << std::endl;
}


static void
print_summary(const jfmap_t *jfplus, const jfmap_t *jfminus,
	      const jlist_t *jlist)
{
  jlist_it_t it;
  int count = 0;
  for (it = jlist->begin(); it != jlist->end(); it++)
  {
    count++;
    std::cout << std::setw(4) << *it << '(';
    std::cout << (jfplus->find(*it) == jfplus->end() ? ' ' : '+');
    std::cout << (jfminus->find(*it) == jfminus->end() ? ' ' : '-');
    std::cout << ')';
    if (count % 10 == 0) std::cout << std::endl;
  }
  std::cout << std::endl;
}


static void
ask_parameters(const jfmap_t *jfplus, const jfmap_t *jfminus,
	       smtinfo_t &info)
{
  bool is_answered = false;
  while (!is_answered)
    {
      std::cout << std::endl << 
	"Input JTOT1, JTOT2, JTOTD and JLPAR (0 for both):" <<
	std::endl;
      std::cin >> info.jtot1 >> info.jtot2 >> info.jtotd >> info.jlpar;
      if (std::cin.fail() || info.jtotd <= 0)
	{
	  std::cin.clear();
	  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	  std::cout << "Invalid input; please try again." << std::endl;
	  continue;
	}
      jfmap_t missplus, missminus;
      jlist_t missjlist;
      for (int jtot = info.jtot1; jtot <= info.jtot2; jtot++)
	{
	  if (info.jlpar >= 0 && jfplus->find(jtot) == jfplus->end())
	    {
	      missjlist.insert(jtot);
	      missplus[jtot] = std::string();
	    }
	  if (info.jlpar <= 0 && jfminus->find(jtot) == jfminus->end())
	    {
	      missjlist.insert(jtot);
	      missminus[jtot] = std::string();
	    }
	}
      if (missjlist.empty()) is_answered = true;
      else
	{
	  std::cout << "Invalid parameters, the folling s-matrices are missing:"
		    << std::endl;
	  print_summary(&missplus, &missminus, &missjlist);
	  std::cout << "Please try again." << std::endl;
	}
    }
}


static void
write_joined_smt(FILE *ofp, const smtinfo_t *info, const std::string filename,
		 int jtot1, int jtot2, int jtotd, int jlpar, bool &is_first)
{
  if (filename == "") return;
  FILE *ifp = fopen(filename.c_str(), "r");
  struct Header head;
  read_header(ifp, &head);
  if (is_first)
    {
      head.jfirst = info->jtot1;
      head.jfinal = info->jtot2;
      head.jtotd = info->jtotd;
      write_header(ofp, &head, 2);
      std::cout << "  header written successfully." << std::endl;
      is_first = false;
    }
  free_header(&head);
  struct SMatrix smt;
  int smt_written = 0;
  while (read_smatrix(ifp, &smt) == 0)
    {
      if (smt.jlpar == jlpar && (smt.jtot - jtot1) % jtotd == 0 &&
	  smt.jtot >= jtot1 && smt.jtot <= jtot2)
	{
	  write_smatrix(ofp, &smt, 2);
	  std::cout << "  s-matrix with jlpar=" << jlpar
		    << " and jtot=" << smt.jtot << " copied from "
		    << filename << std::endl;
	  smt_written += 1;
	}
      free_smatrix(&smt);
    }
  fclose(ifp);
  if (smt_written != (jtot2 - jtot1) / jtotd + 1)
    {
      std::cerr << "joinsmt: error: s-matrix not found." << std::endl;
      exit(-1);
    }
}
 

static void
merge_files(const jfmap_t *jfplus, const jfmap_t *jfminus,
	    const smtinfo_t *info, const char *output_file)
{
  FILE *ofp = fopen(output_file, "w");
  std::cout << "Writing joined s-matrices to " << output_file << std::endl;
  bool is_first = true;
  jfmap_t minus = *jfminus, plus = *jfplus;

  std::string filename_prev = "";
  int_f jtot1;
  if (info->jlpar >= 0)
    for (int_f jtot = info->jtot1; jtot <= info->jtot2 + info->jtotd;
	 jtot += info->jtotd)
      {
	if (jtot == info->jtot2 + info->jtotd ||
	    plus[jtot] != filename_prev)
	  {
	    write_joined_smt(ofp, info, filename_prev, jtot1,
			     jtot - info->jtotd, info->jtotd, 1, is_first);
	    jtot1 = jtot;
	    filename_prev = plus[jtot];
	  }
      }

  filename_prev = "";
  if (info->jlpar <= 0)
    for (int_f jtot = info->jtot1; jtot <= info->jtot2 + info->jtotd;
	 jtot += info->jtotd)
      {
	if (jtot == info->jtot2 + info->jtotd ||
	    minus[jtot] != filename_prev)
	  {
	    write_joined_smt(ofp, info, filename_prev, jtot1,
			     jtot - info->jtotd, info->jtotd, -1, is_first);
	    jtot1 = jtot;
	    filename_prev = minus[jtot];
	  }
      }
}


int run_joinsmt(int argc, char **argv, char *output_file)
{
  jfmap_t jfplus, jfminus;
  jlist_t jlist;
  check_files(argc, argv, jfplus, jfminus, jlist);
  std::cout << std::endl <<
    "The following s-matrices [JTOT(parity)] are found:" << std::endl;
  print_summary(&jfplus, &jfminus, &jlist);
  smtinfo_t sinfo;
  ask_parameters(&jfplus, &jfminus, sinfo);
  merge_files(&jfplus, &jfminus, &sinfo, output_file);
  return 0;
}
