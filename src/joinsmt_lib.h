#ifndef JOINSMT_H
#define JOINSMT_H  1

#include <map>
#include <set>
#include <string>
#include "smatrix.h"

typedef struct
{
  int jtot1, jtot2, jtotd, jlpar;
} smtinfo_t;
typedef std::map<int_f, std::string> jfmap_t;
typedef std::set<int_f> jlist_t;
typedef std::set<int_f>::iterator jlist_it_t;

static void check_files(int, char **, jfmap_t &, jfmap_t &, jlist_t &);
static void print_summary(const jfmap_t *, const jfmap_t *, const jlist_t *);
static void ask_parameters(const jfmap_t *, const jfmap_t *, smtinfo_t &);
static void merge_files(const jfmap_t *, const jfmap_t *, const smtinfo_t *);
static void write_joined_smt(FILE *, const smtinfo_t *, const std::string,
			     int, int, int, int, bool &);

extern "C"
{
  int read_header(FILE *fp, struct Header *head);
  int read_smatrix(FILE *fp, struct SMatrix *smt);
  int write_header(FILE *fp, struct Header *head, int s_version);
  int write_smatrix(FILE *fp, struct SMatrix *smt, int s_version);
  void free_header(const struct Header *head);
  void free_smatrix(const struct SMatrix *smt);
}

extern "C"
{
  int run_joinsmt(int argc, char **argv, char *output_file);
}

#endif
