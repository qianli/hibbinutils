/* sread.c

   Library for reading smt files

   Authors: Qianli Ma

   Revision: Jan 17, 2012 */


#include <string.h>
#include "sread.h"
#include "sqmc01.h"
#include "shib44.h"

int s_version = 0;
int is_j12 = 0;

int check_j12(int_f ibasty)
{
  switch (ibasty)
    {
    case 9:
      return 1;
    case 12:
      return 1;
    case 13:
      return 1;
    case 15:
      return 1;
    case 20:
      return 1;
    case 21:
      return 1;
    case 100:
      return 1;
    default:
      return 0;
    }
}


int read_header(FILE* fp, struct Header *head)
{
  char_f magic_number[9];
  int status;

  head->jlev = NULL;
  head->ilev = NULL;
  head->elev = NULL;
  head->jout = NULL;

  if (fread(magic_number, sizeof(char_f), 8, fp) != 8)
    {
      fputs("read_header: error: unable to read magic number.\n", stderr);
      return -1;
    }

  magic_number[8] = '\0';
  if (!strcmp(magic_number, MAGIC_NUMBER_QMC01))
    {
      s_version = SVER_QMC01;
      is_j12 = 0;
      return read_header_qmc01(fp, head);
    }
  else if (!strcmp(magic_number, MAGIC_NUMBER_HIB44))
    {
      s_version = SVER_HIB44;
      status = read_header_hib44(fp, head);
      is_j12 = check_j12(head->ibasty);
      return status;
    }
  else
    {
      fputs("read_header: error: s-matrix file not recognized.\n", stderr);
      return -2;
    }

  return 0;
}


int write_header(FILE *fp, struct Header *head, int s_version)
{
  switch (s_version)
    {
    case SVER_HIB44:
      return write_header_hib44(fp, head);
      break;
    default:
      fputs("write_header: error: unable to write requested version"
	    "of smt file.\n", stderr);
      return -1;
      break;
    }
}


int write_smatrix(FILE *fp, struct SMatrix *smt, int s_version)
{
  switch (s_version)
    {
    case SVER_HIB44:
      return write_smatrix_hib44(fp, smt);
      break;
    default:
      fputs("write_smatrix: error: unable to write requested version"
	    "of smt file.\n", stderr);
      return -1;
      break;
    }
}


void free_header(const struct Header *head)
{
  if (head->jlev) free(head->jlev);
  if (head->ilev) free(head->ilev);
  if (head->elev) free(head->elev);
  if (head->jout) free(head->jout);
  return;
}


int read_smatrix(FILE *fp, struct SMatrix *smt)
{
  smt->s = NULL;
  smt->j = NULL;
  smt->i = NULL;
  smt->l = NULL;

  switch (s_version)
    {
    case 0:
      fputs("read_smatrix: error: must call read_header first.\n", stderr);
      break;
    case SVER_QMC01:
      return read_smatrix_qmc01(fp, smt);
      break;
    case SVER_HIB44:
      return read_smatrix_hib44(fp, smt, is_j12);
    default:
      fputs("read_matrix: error: s-matrix version not recognized.\n", stderr);
      break;
    }

  return 0;
}


void free_smatrix(const struct SMatrix *smt)
{
  if (smt->s) free(smt->s);
  if (smt->j) free(smt->j);
  if (smt->i) free(smt->i);
  if (smt->l) free(smt->l);
  if (smt->j12) free(smt->j12);
  return;
}
