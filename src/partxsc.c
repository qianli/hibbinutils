/* partxsc.c

   Calculate partial (integral) cross section.

   Authors: Qianli Ma
*/

#include <stdio.h>
#include <math.h>
#include "partxsc.h"

/* Calculate partial cross sections and add to pcs array */
int partial_xsec(struct Header *hp, struct SMatrix *sp, double *pcs)
{
  int nchn, nlev, i, j, ichn, ilev, jchn, jlev, j1, j2;
  double prefact, tmodulus, ksq;
  int *clmap;

  nchn = sp->length;
  nlev = hp->nlevop;

  clmap = (int*)malloc(sizeof(int) * nchn);
  if (clmap == NULL)
    {
      fputs("partxsc: error: insufficient memory.\n", stderr);
      exit(1);
    }
  /* mapping channels to levels */
  for (i = 0; i != nchn; i++)
    {
      for (j = 0; j != nlev; j++)
	{
	  if (sp->j[i] == hp->jlev[j] && sp->i[i] == hp->ilev[j])
	    {
	      clmap[i] = j;
	      break;
	    }
	  if (j == nlev - 1)
	    {
	      fprintf(stderr, "partxsc: error: a channel (%ld, %ld) is not "
		      "found in the level list.\n", sp->j[i], sp->i[i]);
	      exit(1);
	    }
	}
    }
  /* Calculate partial cross sections */
  for (ichn = 0; ichn != nchn; ichn++)
    for (jchn = 0; jchn != nchn; jchn++)
      {
	ilev = clmap[ichn];
	jlev = clmap[jchn];
	tmodulus = (ichn == jchn) ? cabs(1.0 - ELEM(sp->s, ichn, jchn)) :
	  cabs(ELEM(sp->s, ichn, jchn));
	ksq = 2 * hp->rmu * (hp->ered - hp->elev[ilev]);
	if (hp->twomol)
	  {
	    j1 = hp->jlev[ilev] / 10;
	    j2 = hp->jlev[ilev] % 10;
	    if (hp->flaghf)
	      prefact = PI * (2 * sp->jtot + 2) / (2 * j1 + 2) / (2 * j2 + 1);
	    else
	      prefact = PI * (2 * sp->jtot + 1) / (2 * j1 + 1) / (2 * j2 + 1);
	  }
	else
	  {
	    j1 = hp->jlev[ilev];
	    if (hp->flaghf)
	      prefact = PI * (2 * sp->jtot + 2) / (2 * j1 + 2);
	    else
	      prefact = PI * (2 * sp->jtot + 1) / (2 * j1 + 1);
	  }
	prefact /= ksq;
	EMAT(pcs, nlev, ilev, jlev) += prefact * tmodulus * tmodulus;
      }
  free(clmap);
  return 0;
}
