/* shib44.h

   Header file for the library reading stream s-matrix file used in
   hibridon 4.4 (magic number 80 53 4d 54 00 02 00 00)

   Authors: Qianli Ma

   Revision: Aug 22, 2012
*/

#ifndef SHIB44_H
#define SHIB44_H  1

#include <stdio.h>
#include <stdlib.h>
#include "smatrix.h"

#define LEN_OF_DATE_HIB44  20
#define LEN_OF_LABEL_HIB44 48
#define LEN_OF_POTNM_HIB44 48

#define MAGIC_NUMBER_HIB44 "\x80SMT\x00\x02\x00\x00"


struct SMTHeaderHIB44
{
  int_f len_hdr;
  char_f date[LEN_OF_DATE], label[LEN_OF_LABEL], potnm[LEN_OF_POTNM];
  double_f ered, rmu;
  int_f csflag, flaghf, flagsu, twomol, nucros;
  int_f jfirst, jfinal, jtotd, numin, numax;
  int_f nud, nlevel, nlevop, nnout, ibasty;
};


struct SHeaderHIB44
{
  int_f len_sblock, jtot, jlpar, nu, nopen, length, nnout;
};


int read_header_hib44(FILE *fp, struct Header *head);

int read_smatrix_hib44(FILE *fp, struct SMatrix *smt, int is_j12);

int write_header_hib44(FILE *fp, struct Header *head);

int write_smatrix_hib44(FILE *fp, struct SMatrix *smt);

#endif
